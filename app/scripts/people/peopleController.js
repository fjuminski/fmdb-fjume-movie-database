'use strict';

angular.module('people', ['services'])
	.controller('peopleCtrl', ['$scope', 'api', 'globals', function($scope, api, globals) {

		$scope.imageBaseUrl = globals.getImageBaseUrl();
		$scope.shortTextCharacters = 300;

		$scope.attributeExists = function(attribute) {
			if (attribute !== undefined) {
				return true;
			}
			return false;
		};

		$scope.getData = function() {
			api.getPeopleByChart('popular').then(
				function(data) {
					$scope.people = data.data.results;
				},
				function(reason) {
					console.log(reason);
				});
		};

		$scope.getData();
	}]);