'use strict';

angular.module('fmdb', ['ngRoute', 'home', 'movie', 'movies', 'tvshow', 'tvshows', 'person', 'people', 'search', 'account'])
	.controller('mainCtrl', ['$scope', '$location', function($scope, $location) {

		$scope.findTerm = function(term) {
			$location.url('/search/' + term);
		};
	}]);