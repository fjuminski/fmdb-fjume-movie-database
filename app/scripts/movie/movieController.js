'use strict';

angular.module('movie', ['services'])
	.controller('movieCtrl', ['$scope', '$q', '$routeParams', 'api', 'globals', function($scope, $q, $routeParams, api, globals) {

		$scope.imageBaseUrl = globals.getImageBaseUrl();

		$scope.comments = [{
			username: 'megazorkan',
			email: 'tomislav.zoricic@gmail.com',
			timestamp: 1439993400,
			comment: 'Great movie'
		}, {
			username: 'zorkan',
			email: 'tomislav.zoricic@yahoo.hr',
			timestamp: 1439990100,
			comment: 'Awful movie'
		}, {
			username: 'tomislav',
			email: 'tomislav.zoricic@net.com',
			timestamp: 1439991200,
			comment: 'Average movie'
		}, {
			username: 'zoricic',
			email: 'tomislav.zoricic@fer.hr',
			timestamp: 1439993459,
			comment: 'Beautiful movie'
		}, ];

		$scope.addComment = function(username, password, comment) {
			console.log(username + ' ' + password + ' ' + comment);
		};

		$scope.getData = function(id) {
			$q.all([
				api.getMovieInfo(id),
				api.getMovieInfo(id, 'credits')
			]).then(
				function(data) {
					$scope.movies = data[0].data;
					$scope.cast = data[1].data.cast;
				},
				function(reason) {
					console.log(reason);
				});
		};
		$scope.getData($routeParams.id);

	}]);