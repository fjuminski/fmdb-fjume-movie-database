'use strict';

angular.module('movies', ['services'])
	.controller('moviesCtrl', ['$scope', '$routeParams', 'api', 'globals', function($scope, $routeParams, api, globals) {

		$scope.imageBaseUrl = globals.getImageBaseUrl();

		$scope.getData = function(chart) {
			api.getMoviesByChart(chart).then(
				function(data) {
					$scope.movies = data.data.results;
				},
				function(reason) {
					console.log(reason);
				});
		};

		$scope.getData($routeParams.chart);
	}]);