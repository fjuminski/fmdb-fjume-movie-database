'use strict';

angular.module('home', ['services'])
	.controller('homeCtrl', ['$scope', '$q', 'api', 'globals', function($scope, $q, api, globals) {

		$scope.imageBaseUrl = globals.getImageBaseUrl();
		$scope.shortTitleCharacters = 18;
		$scope.getData = function() {
			$q.all([
				api.discover('movie'),
				api.discover('tv')
			]).then(
				function(data) {
					$scope.movies = data[0].data.results;
					$scope.moviesArray = [];
					while ($scope.movies.length) {
						$scope.moviesArray.push($scope.movies.splice(0, 6));
					}

				},
				function(reason) {
					console.log(reason);
				});
		};

		$scope.getData();
	}]);