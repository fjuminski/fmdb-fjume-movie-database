'use strict';

angular.module('person', ['services'])
	.controller('personCtrl', ['$scope', '$routeParams', '$q', 'api', 'globals',
		function($scope, $routeParams, $q, api, globals) {

			$scope.imageBaseUrl = globals.getImageBaseUrl();

			$scope.getData = function(id) {
				$q.all([
					api.getPersonInfo(id),
					api.getPersonInfo(id, 'images'),
					api.getPersonInfo(id, 'movie_credits'),
					api.getPersonInfo(id, 'tv_credits')
				]).then(
					function(data) {
						$scope.basicInformation = data[0].data;
						$scope.images = data[1].data.profiles;
						$scope.movieCredits = data[2].data.cast;
						$scope.tvCredits = data[3].data.cast;
					},
					function(reason) {
						console.log(reason);
					});
			};

			$scope.getData($routeParams.id);
		}
	]);