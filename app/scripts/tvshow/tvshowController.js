'use strict';

angular.module('tvshow', [])
	.controller('tvshowCtrl', ['$scope', '$q', '$routeParams', 'api', function($scope, $q, $routeParams, api) {

		$scope.getData = function(id) {
			$q.all([
				api.getTvshowInfo(id),
				api.getTvshowInfo(id, 'credits'),
				api.getTvshowInfo(id, 'images'),
				api.getTvshowInfo(id, 'similar')
			]).then(
				function(data) {
					$scope.basicInfo = data[0].data;
					$scope.cast = data[1].data.cast;
					$scope.images = data[2].data.posters;
					$scope.similar = data[3].data.results;
				},
				function(reason) {
					console.log(reason);
				})
		}

		$scope.getData($routeParams.id);
	}]);