'use strict';

angular.module('tvshows', ['services'])
	.controller('tvshowsCtrl', ['$scope', 'api', 'globals', function($scope, api, globals) {

		$scope.imageBaseUrl = globals.getImageBaseUrl();

		$scope.getData = function(chart) {
			api.getTvshowsByChart(chart).then(
				function(data) {
					$scope.tvshows = data.data.results;
				},
				function(reason) {
					console.log(reason);
				});
		};

		$scope.getData('popular');
	}]);