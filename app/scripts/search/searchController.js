'use strict';

angular.module('search', ['services'])
	.controller('searchCtrl', ['$scope', '$routeParams', 'api', 'globals', function($scope, $routeParams, api, globals) {

		$scope.imageBaseUrl = globals.getImageBaseUrl();

		$scope.getData = function(term) {
			api.search('multi', term).then(
				function(data) {
					console.log(data);
					$scope.searchResults = data.data.results;
				},
				function(reason) {
					console.log(reason);
				});
		};

		$scope.getData($routeParams.term);
	}]);