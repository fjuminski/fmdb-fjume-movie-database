'use strict';

angular.module('fmdb')
	.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'scripts/home/homeView.html',
				controller: 'homeCtrl'
			})
			.when('/movie/:id', {
				templateUrl: 'scripts/movie/movieView.html',
				controller: 'movieCtrl'
			})
			.when('/movies/:chart', {
				templateUrl: 'scripts/movies/moviesView.html',
				controller: 'moviesCtrl'
			})
			.when('/tvshows', {
				templateUrl: 'scripts/tvshows/tvshowsView.html',
				controller: 'tvshowsCtrl'
			})
			.when('/tvshow/:id', {
				templateUrl: 'scripts/tvshow/tvshowView.html',
				controller: 'tvshowCtrl'
			})
			.when('/people', {
				templateUrl: 'scripts/people/peopleView.html',
				controller: 'peopleCtrl'
			})
			.when('/person/:id', {
				templateUrl: 'scripts/person/personView.html',
				controller: 'personCtrl'
			})
			.when('/search/:term', {
				templateUrl: 'scripts/search/searchView.html',
				controller: 'searchCtrl'
			})
			.when('/account', {
				templateUrl: 'scripts/account/accountView.html',
				controller: 'accountCtrl'
			})
			.otherwise({
				templateUrl: 'scripts/error/errorView.html',
			});
	});