'use strict';

angular.module('services')
	.constant('baseUrl', 'https://fmdbapi.herokuapp.com/comments')
	.factory('backend', function($http, baseUrl) {
		return {
			getComments: function(movieId) {

			},
			postComment: function(user, email, commentText, movieId) {

			},
			deleteComment: function(id) {

			},

			postMovie: function(id, name) {

			}
		}
	});