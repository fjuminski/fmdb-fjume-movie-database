'use strict';

angular.module('services', [])
	.constant('baseUrl', 'http://api.themoviedb.org/3/')
	.constant('apiKey', '25db7eb1def81f6bb554266ca8460b8f')
	.factory('api', function($http, apiKey, baseUrl) {
		return {
			discover: function(category) {
				var url = baseUrl + 'discover/' + category + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},

			search: function(type, term) {
				var url = baseUrl + 'search/' + type + '?api_key=' + apiKey + '&query=' + term;
				return $http.get(url).success(function(data) {
					return data;
				});
			},


			/******************************************
				MOVIES
			*******************************************/
			getMoviesByChart: function(chart) {
				var url = baseUrl + 'movie/' + chart + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},

			getMovieInfo: function(id, info) {
				info === undefined ? info = '' : info = '/' + info;
				var url = baseUrl + 'movie/' + id + info + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},

			/********************
				TV SHOWS
			********************/

			getTvshowsByChart: function(chart) {
				var url = baseUrl + 'tv/' + chart + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},

			getTvshowInfo: function(id, info) {
				info === undefined ? info = '' : info = '/' + info;
				var url = baseUrl + 'tv/' + id + info + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},
			/**********************
				PEOPLE
			**********************/

			getPeopleByChart: function(chart) {
				var url = baseUrl + 'person/' + chart + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},

			getPersonInfo: function(id, info) {
				info === undefined ? info = '' : info = '/' + info;
				var url = baseUrl + 'person/' + id + info + '?api_key=' + apiKey;
				return $http.get(url).success(function(data) {
					return data;
				});
			},
		};

	});