'use strict';

angular.module('services')
	.constant('imageBaseUrl', 'http://image.tmdb.org/t/p/w300/')
	.factory('globals', function(imageBaseUrl) {
		return {
			getImageBaseUrl: function() {
				return imageBaseUrl;
			}
		};
	});