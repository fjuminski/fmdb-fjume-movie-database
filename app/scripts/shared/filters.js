'use strict';

angular.module('services')
	.filter('capitalize', function() {
		return function(word) {
			return word[0].toUpperCase() + word.substr(1);
		};
	})
	.filter('shortenText', function() {
		return function(title, characters) {
			if (title.length > characters) {
				return title.substr(0, characters - 3) + '...';
			}
			return title;
		};
	});